QT += quick
CONFIG += qtquickcompiler
CONFIG += c++14

QMAKE_CFLAGS += -std=gnu11

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        C/InfixToPostfix.c \
        C/stack.c \
        C/EvaluatePostfix.c \
    c_interface.cpp

RESOURCES += qml.qrc

HEADERS += \
    c_interface.h

DISTFILES += \
    qtquickcontrols2.conf \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
